基于gitlab-runner，karma，chromeheadless的ci解决方案

## 注意问题

### 如果runner环境直接运行，可能会出现错误：

```log
02 11 2019 05:57:17.772:ERROR [launcher]: Cannot start ChromeHeadless
	/builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.772:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 05:57:17.773:ERROR [launcher]: ChromeHeadless stderr: /builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.775:INFO [launcher]: Trying to start ChromeHeadless again (1/2).
02 11 2019 05:57:17.781:ERROR [launcher]: Cannot start ChromeHeadless
	/builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.782:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 05:57:17.782:ERROR [launcher]: ChromeHeadless stderr: /builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.783:INFO [launcher]: Trying to start ChromeHeadless again (2/2).
02 11 2019 05:57:17.788:ERROR [launcher]: Cannot start ChromeHeadless
	/builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.788:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 05:57:17.788:ERROR [launcher]: ChromeHeadless stderr: /builds/ncfe/vue.qeditor/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

02 11 2019 05:57:17.789:ERROR [launcher]: ChromeHeadless failed 2 times (cannot start). Giving up.
npm ERR! Test failed.  See above for more details.
ERROR: Job failed: exit code 1
```

### 如果使用解决方案yml的script运行，并且使用share的docker runner：
```bash
apt-get update
apt-get install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
```

出现以下错误

```log
02 11 2019 06:03:47.182:INFO [launcher]: Launching browser ChromeHeadless with unlimited concurrency
02 11 2019 06:03:47.185:INFO [launcher]: Starting browser ChromeHeadless
02 11 2019 06:03:47.908:ERROR [launcher]: Cannot start ChromeHeadless
	[1102/060347.872141:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:47.909:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 06:03:47.910:ERROR [launcher]: ChromeHeadless stderr: [1102/060347.872141:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:47.912:INFO [launcher]: Trying to start ChromeHeadless again (1/2).
02 11 2019 06:03:47.956:ERROR [launcher]: Cannot start ChromeHeadless
	[1102/060347.952626:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:47.957:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 06:03:47.957:ERROR [launcher]: ChromeHeadless stderr: [1102/060347.952626:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:47.958:INFO [launcher]: Trying to start ChromeHeadless again (2/2).
02 11 2019 06:03:48.003:ERROR [launcher]: Cannot start ChromeHeadless
	[1102/060347.997557:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:48.004:ERROR [launcher]: ChromeHeadless stdout: 
02 11 2019 06:03:48.004:ERROR [launcher]: ChromeHeadless stderr: [1102/060347.997557:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.

02 11 2019 06:03:48.005:ERROR [launcher]: ChromeHeadless failed 2 times (cannot start). Giving up.
npm ERR! Test failed.  See above for more details.
ERROR: Job failed: exit code 1
```

### 解决方案是，使用自定义的runner，在runner运行：
#### 运行[命令](https://gitlab.com/gitlab-org/gitlab-runner/issues/2493):
```n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr/local```

