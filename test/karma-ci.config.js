var browsers = require('./browsers');

process.env.CHROME_BIN = require('puppeteer').executablePath()
module.exports = function(config) {
  config.set({
    basePath: '../',
    urlRoot: '/karma/',
    port: process.env.npm_package_config_ports_karma,

    files: [
      { pattern: 'dist/unit.js', nocache: true },
      { pattern: 'dist/*.map', included: false, served: true, nocache: true }
    ],
    proxies: {
      '/assets/': '/karma/base/assets/'
    },

    frameworks: ['jasmine'],
    reporters: ['progress'],
    colors: true,
    autoWatch: false,
    singleRun: true,
    browsers: ['ChromeHeadless'],

    client: {
      useIframe: false
    },

    coverageReporter: {
      dir: '.coverage',
      reporters: [
        { type: 'text' },
        { type: 'html' }
      ]
    },
    customLaunchers: browsers
  });
};
