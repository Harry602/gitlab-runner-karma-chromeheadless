import $ from 'jquery';
import App from '../src/app'

describe('Some unit test', function() {
    it('test click', async function() {
        App();
        $('#button').click();
        expect($('#info').html()).toEqual('click');
    });
});