const path = require('path');

module.exports = function (env) {
  var config = {
    entry: {
      'bundle.js': './src/app.js',
      'unit.js': './test/unit.js'
    },

    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name]',
      publicPath: '/dist/'
    },

    mode: 'development',
    module: {
      rules: [
        {
          test: /.js$/,
          use: [{
            loader: 'babel-loader',
            options: {
              ...(env && env.coverage ? { plugins: ['istanbul', 'transform-es2015-modules-commonjs'] } : {
                presets: [
                  ["@babel/env", {
                    "targets": {
                      "browsers": "last 2 chrome versions"
                    }
                  }]
                ]
              })
            }
          }]
        },
      ]
    },

    resolve: {
      extensions: ['.json', '.js']
    },

    devtool: 'source-map',
  };
  return config
}